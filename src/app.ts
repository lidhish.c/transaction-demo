import express, {Express} from 'express';
import Helmet from 'helmet';
import cors from 'cors';
import {router} from './demo';
const app: Express = express();

app.use(cors());
app.use(Helmet());
app.use(express.urlencoded());
app.use(express.json()); // if needed
app.use('/', router);
export default app;
