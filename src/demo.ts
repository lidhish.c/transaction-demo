import {AppDataSource} from '../db/data-source';
import {User} from '../db/entity/User';
import {Request, Response, Router} from 'express';
import {QueryRunner, Repository} from 'typeorm';

export const router = Router();
const transferAmount = 100;

router.get('/get-users', (_req: Request, res: Response) => {
  const userRepo: Repository<User> = AppDataSource.getRepository(User);
  try {
    userRepo.find().then((users: User[]) => {
      res.status(200).json(users);
    });
  } catch (error) {
    res.status(500).json({message: error});
  }
});

router.post('/create-user', async (req: Request, res: Response) => {
  const userRepo: Repository<User> = AppDataSource.getRepository(User);
  const body = req.body;
  const userData = await userRepo.save(body);
  res.status(200).send(userData);
});

// with out transaction
router.post('/transfer-balance', async (req: Request, res: Response) => {
  // send money from user1 to user2

  try {
    const userRepo: Repository<User> = AppDataSource.getRepository(User);
    const {user1Id, user2Id}: {[key: string]: string} = req.body;

    const user1Data = await userRepo.findOneBy({id: user1Id});
    const user2Data = await userRepo.findOneBy({id: user2Id});

    // debiting 100 from user 1
    await userRepo.update(
      {id: user1Id},
      {balance: user1Data.balance - transferAmount}
    );

    // throw new Error('System Error');

    // crediting 100 from user 2
    await userRepo.update(
      {id: user2Id},
      {balance: user2Data.balance + transferAmount}
    );

    res.status(200).send({message: 'Transfer completed'});
  } catch (error) {
    res.status(500).json({message: 'Balance transfer Error'});
  }
});

router.post(
  '/transfer-balance-transaction1',
  async (req: Request, res: Response) => {
    // send money from user1 to user2
    const queryRunner: QueryRunner = AppDataSource.createQueryRunner();

    try {
      queryRunner.startTransaction();
      const userRepo: Repository<User> =
        queryRunner.manager.getRepository(User);
      const {user1Id, user2Id}: {[key: string]: string} = req.body;

      const user1Data = await userRepo.findOneBy({id: user1Id});
      const user2Data = await userRepo.findOneBy({id: user2Id});

      // debiting 10 from user 1
      await userRepo.update(
        {id: user1Id},
        {balance: user1Data.balance - transferAmount}
      );

      //throw new Error('System Error');

      // crediting 10 from user 2
      await userRepo.update(
        {id: user2Id},
        {balance: user2Data.balance + transferAmount}
      );
      queryRunner.commitTransaction();
      res.status(200).send({message: 'Transfer completed'});
    } catch (error) {
      queryRunner.rollbackTransaction();
      res.status(500).json({message: 'Balance transfer Error'});
    }
  }
);

router.post(
  '/transfer-balance-transaction2',
  async (req: Request, res: Response) => {
    // send money from user1 to user2

    AppDataSource.transaction(async manager => {
      const {user1Id, user2Id}: {[key: string]: string} = req.body;
      const userRepo: Repository<User> = manager.getRepository(User);

      const user1Data = await userRepo.findOneBy({id: user1Id});
      const user2Data = await userRepo.findOneBy({id: user2Id});

      // debiting 10 from user 1
      await userRepo.update(
        {id: user1Id},
        {balance: user1Data.balance - transferAmount}
      );

      //throw new Error('System Error');

      // crediting 10 from user 2
      await userRepo.update(
        {id: user2Id},
        {balance: user2Data.balance + transferAmount}
      );
      res.status(200).send({message: 'Transfer completed'});
    }).catch(() => {
      res.status(500).json({message: 'Balance transfer Error'});
    });
  }
);
