# Awesome Project Build with TypeORM

Steps to run this project:

1. Run `yarn install` command
2. Setup database settings inside `data-source.ts` file
3. Update .env file as a reference of .env.sample file
4. Run yarn run dev

note:

If you want you use postgres docker image to spin up the database, feel free to run

`docker compose up`
